/**
 *   This file is part of wald:data - the storage back-end of wald:meta.
 *   Copyright (C) 2016  Kuno Woudt <kuno@frob.nl>
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of copyleft-next 0.3.1.  See copyleft-next-0.3.1.txt.
 */

'use strict';

const bodyParser = require('body-parser');
const edit = require('./edit');
const express = require('express');
const find = require('wald-find');
const fs = require('fs');
const httpinvoke = require('httpinvoke');
const URI = require('urijs');
const when = require('when');

const configFile = process.env.WALD_DATA_CONFIG_FILE
    ? process.env.WALD_DATA_CONFIG_FILE
    : __dirname + '/../test/test-config.ttl';

// const a = find.a;
const cs = find.namespaces.cs;
// const rdf = find.namespaces.rdf;

function error(response, err) {
    console.log('ERROR: ', err);
    response.status(500);
    response.send(
        '<html>' +
            '    <head><title>500 Internal Server Error</title></head>' +
            '    <body>' +
            '        <h1>500 Internal Server Error</h1>' +
            '        <p>' +
            err.toString() +
            '</p>' +
            '    </body>' +
            '</html>'
    );
}

function redirect(response, url) {
    response.header('Location', url);
    response.status(303);
    response.send(
        '<html>' +
            '    <head><title>303 See Other</title></head>' +
            '    <body>' +
            '        <h1>303 See Other</h1>' +
            '        <p>Please see <a href="' +
            url +
            '">' +
            url +
            '</a></p>' +
            '    </body>' +
            '</html>'
    );
}

class Edits {
    constructor(entities) {
        this.entities = entities;
        this.fetch = this.fetch.bind(this);
        this.create = this.create.bind(this);
    }

    fetch(request, response) {
        const cfg = edit.entityConfiguration(this.entities);
        const id = URI(cfg.baseUri).path(request.url);

        const ldfSearch = URI(cfg.baseUri).path('/data/edits').query({
            subject: id.toString(),
            predicate: '',
            object: '',
        });

        redirect(response, ldfSearch.toString());
    }

    create(request, response) {
        edit.processChangeSet(this.entities, request.body).then(result => {
            const cfg = edit.entityConfiguration(this.entities);
            const fusekiUrl = URI('http://localhost:3030').path(
                cfg.dataset + '/update'
            );

            return when(
                httpinvoke(fusekiUrl.toString(), 'POST', {
                    headers: { 'Content-Type': 'application/sparql-update' },
                    input: result.update,
                })
            )
                .then(function(fusekiResponse) {
                    if (
                        fusekiResponse.statusCode !== 200 &&
                        fusekiResponse.statusCode !== 204
                    ) {
                        if (fusekiResponse.body) {
                            error(response, fusekiResponse.body);
                        } else {
                            error(response, fusekiResponse.statusCode);
                        }
                    } else {
                        // update processed correctly
                        redirect(response, result.id);
                    }
                })
                .catch(function(err) {
                    error(response, err);
                });
        });
    }
}

function buildRoutes(datastore) {
    const cfg = edit.entityConfiguration(datastore);

    const editResource = cfg.types[cs.ChangeSet];
    if (!editResource) {
        throw new Error('No type cs:ChangeSet type specified');
    }

    const editCollection = cfg.plurals[editResource];
    if (!editCollection) {
        throw new Error('No plural specified for ', editResource);
    }

    const edits = new Edits(datastore);

    const routes = { GET: {}, POST: {} };
    // FIXME: add content-negotation based on suffix (e.g. /edit/ed123.json, /edit/ed123.ttl)
    routes.GET['/' + editResource + '/:id'] = edits.fetch;
    routes.POST['/' + editCollection + '/?'] = edits.create;

    return routes;
}

function factory() {
    const configBody = fs.readFileSync(configFile, 'UTF-8');

    return find.tools.parseTurtle(configBody).then(function(datastore) {
        const routes = buildRoutes(datastore);
        const app = express();

        app.use(bodyParser.text({ limit: '1024kb', type: 'text/*' }));

        Object.keys(routes).forEach(method => {
            Object.keys(routes[method]).forEach(path => {
                app[method.toLowerCase()](path, routes[method][path]);
            });
        });

        return app;
    });
}

module.exports = {
    buildRoutes: buildRoutes,
    Edits: Edits,
    factory: factory,
};

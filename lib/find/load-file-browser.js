/**
 *   This file is part of wald:find - a library for querying RDF.
 *   Copyright (C) 2016  Kuno Woudt <kuno@frob.nl>
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of copyleft-next 0.3.1.  See copyleft-next-0.3.1.txt.
 */

'use strict';

const httpinvoke = require('httpinvoke');
const when = require('when');

function loadFile(iri) {
    return when(httpinvoke(iri, 'GET')).then(function(data) {
        return data.body;
    });
}

module.exports = loadFile;
